const express = require('express');
const cors = require('cors');

const mongoose = require('mongoose');
const requireDir = require('require-dir');

const app = express();

app.use(express.json());
app.use(cors());

const connectionString = 'mongodb+srv://perrony:y2n0o3R4@cluster0-apx5s.mongodb.net/nodeapi?retryWrites=true&w=majority';

mongoose.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true} )
.then(() => {
  console.log('Conectado na base de dados');
  app.emit('pronto');
})
.catch(e => console.log(e));

requireDir('./src/model');



app.use('/api', require('./src/routes'));

app.on('pronto', () => {
  app.listen(3001, () => {
    console.log('Acessar http://localhost:3001');
  });
}) 

