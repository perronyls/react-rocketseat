import api from './api';


class App {
  constructor(){
    this.repositories = [];
    this.formEl = document.querySelector('#repo-form');
    this.listEl = document.querySelector('#repo-list');
    this.inputEl = document.querySelector('input[name=repository]');
    this.registerHandlers();
  }

  registerHandlers() {
    this.formEl.onsubmit = e => this.addRepository(e);
  }

  setLoading(loading = true){
    if(loading){
      let loadingEl = document.createElement('span');
      loadingEl.appendChild(document.createTextNode('Carregando...'));
      loadingEl.setAttribute('id','loading');

      this.formEl.appendChild(loadingEl);

    } else {
      document.getElementById('loading').remove();
    }
  }

  async addRepository(e) {

    e.preventDefault();

    this.setLoading();

    const repoInput = this.inputEl.value;

    if(!repoInput) return;

    try {

      const response = await api.get(`/users/${repoInput}`);
      
      const { login, url, avatar_url, html_url } = response.data;

      this.repositories.push({
        login,
        url,
        avatar_url,
        html_url,
      }); 

      this.inputEl.value = '';

      this.render();

  } catch(error){
    alert('Este perfil não existe!');
  }
  this.setLoading(false);
  }

  render() {
    this.listEl.innerHTML = '';

    this.repositories.forEach(repo => {
      let imgEl = document.createElement('img');
      imgEl.setAttribute('src',repo.avatar_url);

      let titleEl = document.createElement('strong');
      titleEl.appendChild(document.createTextNode(repo.login));

      let descriptionEl = document.createElement('p');
      descriptionEl.appendChild(document.createTextNode(repo.url));

      let linkEl = document.createElement('a');
      linkEl.setAttribute('target','_blank');
      linkEl.setAttribute('href',repo.html_url); 
      linkEl.appendChild(document.createTextNode('Acessar'));

      let listItemEl = document.createElement('li');
      listItemEl.appendChild(imgEl);
      listItemEl.appendChild(titleEl);
      listItemEl.appendChild(descriptionEl);
      listItemEl.appendChild(linkEl);

      this.listEl.appendChild(listItemEl);
    })
  }

  
}

new App();